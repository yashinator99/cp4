/**
 * Name: Yash Khanchandani
 * Date: August 11, 2020
 * Section: CSE 154 AC
 * This is the JS to implement the UI for my cryptogram generator, and generate
 * different types of ciphers from user input.
 */
/**
 * Global Varibles
 */
"use strict";
let blah = "/convert";
let boo = "/helpdesk";

(function() {
  window.addEventListener('load', init);

  /**
   * this is the function that call the function to load
   */
  function init() {
    setInputFilter(document.getElementById("b-iput1"), function(value) {
      return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
    });
    let submitbutt = document.getElementById("sumbitbutton");
    let togglebutt = document.getElementById("togglebutton");
    submitbutt.addEventListener("click", submitdata);
    togglebutt.addEventListener("click", convertTable);
  }

  /**
   * this function sumbits the value of input to the api
   * @param {Event} event event to prevent the disappearance of input
   */
  function submitdata(event) {
    event.preventDefault();
    let units = document.getElementById("conver").value;
    let inputVal = document.getElementById("b-iput1").value;
    let urlBlah = blah + "?unit=" + units + "&val=" + inputVal;
    fetch(urlBlah)
      .then(checkStatus)
      .then(resp => resp.json())
      .then(updatingResults)
      .catch(handleError);
  }

  /**
   * this function calls the api for quick guide to convert
   * @param {Event} event to prevent any disappearances
   */
  function convertTable(event) {
    event.preventDefault();
    let text = document.getElementById("helpdiv");
    text.classList.toggle("hidden");
    if (!text.classList.contains("hidden")) {
      fetch(boo)
        .then(checkStatus)
        .then(resp => resp.text())
        .then(covertHelp)
        .catch(handleError);
    }
  }

  /**
   * this check if it is ok
   * @param {Object} response this is the url to check
   * @returns {Object} this is the url
   */
  function checkStatus(response) {
    if (response.ok) {
      return response;
    } else {
      throw Error("Error in request: " + response.statusText);
    }
  }

  /**
   * this is the error function
   * @param {String} err This is the string of the error
   * @returns {String} err is the string
   */
  function handleError(err) {
    return err;
  }

  /**
   * this updates the output and the units under the text boxes
   * @param {JSON} data the Game Data
   */
  function updatingResults(data) {
    let outVal = document.getElementById("oputs").children[1];
    let unitName1 = document.getElementById("iputs").children[2];
    let unitName2 = document.getElementById("oputs").children[2];
    outVal.value = data.val.toFixed(3);
    unitName1.innerText = data.unit1;
    unitName2.innerText = data.unit2;
  }

  /**
   * this updates the table
   * @param {String} data the string for the quick conversion
   */
  function covertHelp(data) {
    let text = document.getElementById("helptable");
    text.innerText = data;
  }

  /**
   * this prevents the inputbox to accept any character that are not 0-9 and "."
   * @param {somethin} textbox this is the textbox
   * @param {asd} inputFilter this is the filter
   */
  function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"]
      .forEach(function(event) {
        textbox.addEventListener(event, function() {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (Object.prototype.hasOwnProperty.call(this, "oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          } else {
            this.value = "";
          }
        });
      });
  }

})();