# Metric Conversion API API Documentation
This api is a metric conversion system and a quick guide for converting the values

## helpdesk
**Request Format:** http://<url>:8000/helpdesk

**Request Type:** GET

**Returned Data Format**: Plain Text

**Description:** This Endpoint provides the formulas for the conversion


**Example Request:** http://localhost:8000/helpdesk

**Example Response:**

```
Quick Converstion:
 Kilograms to Pounds: Kilograms * 2.2
Pounds to Kilograms: Pounds / 2.2
 Kilometers to Miles: Kilometers * 0.62
Miles to Kilometers: Miles / 0.62
 Celsius to Fahrenheit: (Celsius * 1.8) + 32
Fahrenheit to Celsius: (Fahrenheit - 32) / 1.8
```

**Error Handling:**
It will return the HTTP error codes

## convert
**Request Format:** http://<url>:8000/convert?unit=<units>&val=<inputVal>

**Request Type:** GET

**Returned Data Format**: JSON

**Description:** This Endpoint convert one unit to another unit value

**Example Request:** http://localhost:8000/convert?unit=kgs&val=12

**Example Response:**

```json
{
"unit1":"Kilograms","unit2":"Pounds","val":26.4552
}
```

**Error Handling:**
It will return the HTTP error codes
