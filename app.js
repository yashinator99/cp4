/**
 * Name: Yash Khanchandani
 * Date: August 9, 2020
 * Section: CSE 154 AC
 * This is the JS to implement the UI for my cryptogram generator, and generate
 * different types of ciphers from user input.
 */
/**
 * Global Variables
 */
"use strict";
const PORTNUM = 8000;
const PORT = process.env.PORT || PORTNUM;
const KTOP = 2.2046;
const KTOM = 0.62137;
const CTOF1 = 1.8000;
const CTOF2 = 32.00;
const express = require('express');
const app = express();
app.use(express.static('public'));
let outputObj = "";

/**
 * this is the api of json which calls the function to calculate the value and put it into a json
 */
app.get('/convert', function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  let unit = req.query['unit'];
  let value = req.query['val'];
  if (unit === "kgs") {
    kgs(value);
  }
  if (unit === "lbs") {
    lbs(value);
  }
  if (unit === "km") {
    km(value);
  }
  if (unit === "miles") {
    miles(value);
  }
  if (unit === "celsius") {
    celsius(value);
  }
  if (unit === "fahrenheit") {
    fahrenheit(value);
  }
  if (value === "") {
    outputObj["val"] = "";
  }
  res.type("json").send(outputObj);
});

/**
 * this is the api for text and it return the text instead of json
 */
app.get('/helpdesk', function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  let returnStr = "Quick Converstion: \n Kilograms to Pounds: Kilograms * 2.2 \n" +
    "Pounds to Kilograms: Pounds / 2.2 \n Kilometers to Miles: Kilometers * 0.62 \n" +
    "Miles to Kilometers: Miles / 0.62 \n Celsius to Fahrenheit: (Celsius * 1.8) + 32 \n" +
    "Fahrenheit to Celsius: (Fahrenheit - 32) / 1.8";
  res.type("text").send(returnStr);
});

/**
 * convert the input which is kilograms to pounds
 * @param {Integer} value the number form input box
 */
function kgs(value) {
  let outputVal = value * KTOP;
  outputObj = {"unit1": "Kilograms", "unit2": "Pounds", "val": outputVal};
}

/**
 * convert the input which is pounds to kilograms
 * @param {Integer} value the number form input box
 */
function lbs(value) {
  let outputVal = value / KTOP;
  outputObj = {"unit1": "Pounds", "unit2": "Kilograms", "val": outputVal};
}

/**
 * convert the input which is kilometer to miles
 * @param {Integer} value the number form input box
 */
function km(value) {
  let outputVal = value * KTOM;
  outputObj = {"unit1": "Kilometers", "unit2": "Miles", "val": outputVal};
}

/**
 * convert the input which is mile to kilometers
 * @param {Integer} value the number form input box
 */
function miles(value) {
  let outputVal = value / KTOM;
  outputObj = {"unit1": "Miles", "unit2": "Kilometers", "val": outputVal};
}

/**
 * convert the input which is celsius to fahrenheit
 * @param {Integer} value the number form input box
 */
function celsius(value) {
  let outputVal = (value * CTOF1) + CTOF2;
  outputObj = {"unit1": "Celsius", "unit2": "Fahrenheit", "val": outputVal};
}

/**
 * convert the input which is fahrenheit to celsius
 * @param {Integer} value the number form input box
 */
function fahrenheit(value) {
  let outputVal = (value - CTOF2) / CTOF1;
  outputObj = {"unit1": "Fahrenheit", "unit2": "Celsius", "val": outputVal};
}

app.listen(PORT);